# Libra

Libra is a tool built in R to calibrate batch effects of LC-MS based metabolomics data.
Libra is developed during Tu's Master's thesis (bfi-wholegrain), supervised by Lars Dragsted and Gözde Gürdeniz.